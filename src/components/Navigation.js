import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navigation.css'
const Navigation = () => {
    return (
        <div className="navigation">
            <NavLink exact to="/" activeClassName="nav-active">Accueil</NavLink>
            <NavLink exact to="/gallerie" activeClassName="nav-active">Gallerie</NavLink>            
        </div>
    );
};

export default Navigation;