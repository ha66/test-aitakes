
import React from 'react'
import { Link } from 'react-router-dom'
import './Details.css'

function Details (props) {

    return (
        <>
            <Link to={{pathname: "/gallerie"}}>Retour</Link>
            <h1>Detail du livre</h1>
            <div className="wrapper">
                <img src={props.location.imgUrl} alt={props.location.titre} titre={props.location.titre}/>
                <ul>
                    <li><h2>{props.location.titre}</h2></li>
                    <li><span className="description">Description : </span>{props.location.description}</li>
                    <li><span className="auteur">Auteur : </span>{props.location.auteur}</li>
                </ul>
            </div>
        </>
    )
}
export default Details