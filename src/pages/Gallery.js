import React, {useState, useEffect} from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Gallery.css';
import Navigation from '../components/Navigation';
const Gallery = () => {

    const [books, setBooks] = useState([]);
    
    useEffect(() => {
        const fetchBooks = async () => {
            const [res] = await Promise.all([axios.get(`https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=${process.env.REACT_APP_BOOKS_API_KEY}`)])
            //console.log(res.data.results.books);
            setBooks(res.data.results.books)
        }
    fetchBooks();

    }, [])

    return (
        <>
            <h1>NOS BEST-SELLERS</h1> 
            <section className="liste">
               {
               books.map((book) => {
                    const { rank } = book
                    return(
                        <article key={rank}>
                            <Link to={{
                                pathname: `details/${rank}`,                              
                                imgUrl:book.book_image,
                                titre:book.title,
                                auteur:book.author,
                                description:book.description
                                }}>
                                <img src={book.book_image} alt={book.title} title={book.title} />
                            </Link>
                            <div>
                                <h3>{book.title}</h3>
                            </div>
                        </article>
                        )
                    }
               )}
            </section>
            <Navigation />
        </>
    );
};

export default Gallery;