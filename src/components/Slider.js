import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import './Slider.css';

function Slider() {

    const imagesUrl = 'http://react-responsive-carousel.js.org/assets/';
    
    const datas = [
        {
            id: 1,
            image: `${imagesUrl}1.jpeg`,
            title: "Un exemple pour les hommes",
            description: "Une leçon d'abnégation que nous donnent ces petits êtres pour le travail d'équipe"
        },
        {
            id: 2,
            image: `${imagesUrl}2.jpeg`,
            title: "Un bureau de rêve",
            description: "Idéal pour le travail en distanciel !"
        },
        {
            id: 3,
            image: `${imagesUrl}3.jpeg`,
            title: "Pour les fans de pêche",
            description: "Maginifique spot pour la pêche à la daurade et autres sparidés"
        }
    ]

    return (
        <Carousel
        autoPlay 
        infiniteLoop 
        interval={5000}
        showThumbs={false}
        showIndicators={false}
        showStatus={false}
        showArrows={false}
        >
            {datas.map(slide => (
                <div key={slide.id}>
                    <img src={slide.image} alt='' />
                    <div className="wrapper">
                        <ul>
                            <li><h3>{slide.title}</h3></li>
                            <li>{slide.description}</li>
                        </ul>

                    </div>
                </div>
            ))}
        </Carousel>
    );
};

export default Slider;