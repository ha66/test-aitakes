import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return (
        <div>
            <h1>Erreur 404</h1>
            <p><Link to ='/'>Retour à la gallerie</Link></p>
        </div>   
    );
};

export default NotFound