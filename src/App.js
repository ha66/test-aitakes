import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Gallery from './pages/Gallery';
import Details from './pages/Details';
import Home from './pages/Home';
import NotFound from './pages/NotFound';

function App() {

  return (

      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/gallerie" exact component={Gallery} />
          <Route path="/details/:id" exact component={Details} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>

  )
}

export default App;
