
import Slider from '../components/Slider';
import Navigation from '../components/Navigation';
        

function App() {
  return (
    <div className="App">
      <Slider />
      <Navigation />
    </div>
  );
}

export default App;